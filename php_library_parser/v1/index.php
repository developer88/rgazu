﻿<?php
/**
 * RGAZU Library Files Parser v1.9
 */

//settings
$doc_converter_path = "C:\\APPS_installed\\Convert_Doc\\ConvertDoc.exe";
$url = "/img/library/newentries/"; //url to files for website
$html_ext = "htm"; //extention for HTML files with data
$dir = "files"; // Dir where all images will be placed
$server_side_folders_array = Array(
    "0" => "agriculture",
    "1" => "applied_science",
    "2" => "economics",
    "3" => "jurisprudence",
    "4" => "other",
    "5" => "science",
    "6" => "socialscience",
    "7" => "tech"   
);

//START 

draw_interface();

if(isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "prepare":
            prepare_files($doc_converter_path);
            break;
        case "parce":
            parse_files($dir,$url,$html_ext,$server_side_folders_array);
            break;
    }
}

//============================================================

/**
 * Draw an interface
 */
function draw_interface()
{
    echo "<h3>RGAZU Library Files Parser v1.9</h3>";
    echo "
   <!-- <form>
        <input type='hidden' name='action' value='prepare'/>
        1. <input type='submit' value='Подготовить файлы'/>
		<br/>Нужно положить *.DOC файлы в папку raw_files
    </form>-->
    <form>
        <input type='hidden' name='action' value='parce'/>       
        1. <select name='path'>
            <option value='0'>Сельское хозяйство</option>
            <option value='1'>Комплексные и прикладные науки</option>
            <option value='2'>Экономика</option>
            <option value='3'>Юридические науки</option>
            <option value='4'>Разное</option>
            <option value='5'>Естественные науки</option>
            <option value='6'>Социальные науки</option>
            <option value='7'>Технические науки</option>
        </select>
		<br/>Нужно положить конвертировать сначала файлы DOC в HTML и в TXT, а потом положить в папку files<br/>
        <input type='submit' value='Начать парсинг файлов'/>
    </form>
    ";   
}

/**
 * Draw a message
 * @param text $text 
 */
function draw_message($text)
{
    echo "<div style='color:green;'>$text</div>";    
}

function prepare_files($doc_converter_path)
{
    $file_count = 0;
    if(!file_exists("raw_files/"))
        mkdir("raw_files/","0777");
    
    if(file_exists("files/"))
        remove_directory("files/");
        
    mkdir("files/", "0777");       
    
    $doc_files = get_dirlist("raw_files", "doc");    
    
    if(count($doc_files) == 0)
    {
        draw_message("No files to be prepared! Finished");
        return "";
    }    

    $ff= '';
    foreach ($doc_files as $file) {
        $new_file_name =  str_replace("doc", "", $file) . "doc";
        if(!file_exists("raw_files/$new_file_name"))
            copy("raw_files/$file", "raw_files/$new_file_name");
        if($file != $new_file_name)
        {
            unlink("raw_files/$file");
            $file_count++;
        }
        $ff = $new_file_name;
    }

 //   SaveAsHtmlByWord("F:/Servers/xampp/htdocs/Php_stuff/RGAZU_LIBRAY_PARSER/raw_files/".$ff, "F:/Servers/xampp/htdocs/Php_stuff/RGAZU_LIBRAY_PARSER/raw_files/gggg.htm");
    
    
    $command = $doc_converter_path .' /S "'.  __DIR__ .'\\raw_files\\*.DOC" /T "'.  __DIR__  .'\\files\\*.HTM" /F9 /C4 /M2 /V';
    echo $command."<br/>";
   // exec ($command, $arrACL ); 
    
    draw_message("Preparing files finished! Proceeded: $file_count files");    
}

/**
 * Parce files from html into one index.html
 */
function parse_files($dir,$url,$html_ext,$server_side_folders_array)
{
    $file_count = 0;
    if(file_exists("output/".$dir))
        remove_directory("output/$dir");
        
    mkdir("output/".$dir, "0777");    

    //index file content
    $result_file = "";

    //get_template
    $template = rfile("templates/library_200312.html");

    //get dir
    $html_files = get_dirlist("files", $html_ext);
   
    //TODO fix regexps - they are UGLY now!!!
    foreach ($html_files as $file) {
        $content = rfile("files/".$file);
		$text = rfile("files/".str_replace($html_ext,"txt",$file));
        $arr = preg_split("<div class=WordSection1>",$content);
        $arr2 = preg_split("<div class=Section1>",$content);
        if(count($arr)>1 || count($arr2)>1)
        {
			if(count($arr)>1){
				$str = $arr[1];
			}else{
				$str = $arr2[1];
			}
			
			//обрабатываем файл с текстом
			$content_to_proceeed = $text;
			$content_to_proceeed = str_replace("  ", "", $content_to_proceeed);
			$content_to_proceeed = str_replace("\r\n", "#", $content_to_proceeed);
			$content_to_proceeed = str_replace("##", "", $content_to_proceeed);
			//$content_to_proceeed = str_replace("# #", "", $content_to_proceeed);
			$content_to_proceeed = str_replace("    ", "", $content_to_proceeed);
			$content_to_proceeed = str_replace("  ", "", $content_to_proceeed);
			//$content_to_proceeed = str_replace(" # #", "", $content_to_proceeed);
			$content_to_proceeed = str_replace("#", "splt", $content_to_proceeed);			
			$arr_orig = preg_split("/splt/",$content_to_proceeed);
			$arr = Array();
			for($i = 0; $i < count($arr_orig); $i++){
				if(strlen(trim($arr_orig[$i]))>1){
					array_push($arr,$arr_orig[$i]);
				}
			}
			$text_content = "";
			$caption = "";
			$num1 =  trim($arr[0]);
			$num2 =  trim($arr[1]);
			array_shift($arr);
			array_shift($arr);
			
			if(strlen($arr[count($arr)-1])<3){
				array_pop($arr);
			}			
			
			if(count($arr) == 1){
				$caption = $arr[0];
			}
			if(count($arr) == 2){
				if(strlen($arr[1]) < 3){
					$caption = implode(" ", $arr);
				}else{
					$caption = $arr[0];
					$text_content = $arr[1];
				}			
			}
			if(count($arr) == 3){
				$caption =  $arr[0];
				array_shift($arr);	
				$text_content = implode(" ", $arr);		
			}			
			if(count($arr) > 3){
				$caption = $arr[0]." ".$arr[1];
				array_shift($arr);
				array_shift($arr);	
				$text_content = implode(" ", $arr);		
			}
			//$text_content =  count($arr).implode("!@@@@@@@@@@@@@@ ", $arr);

			//Копируем картинку
            $img = "";
            $img_path = "files/". str_replace(".".$html_ext, "", $file)."_files/image001.jpg";
			if(file_exists("output/".$dir."/") == false){
				mkdir("output/".$dir);
			}
            if(file_exists($img_path))
            {
				$img_filename = date('YmdGi')."_".strlen(str_replace(".".$html_ext, "", $file)).rand(0,23456).".jpg";
                $img = "output/".$dir."/".$img_filename;
                copy($img_path, $img);
                $img_path = $url.$server_side_folders_array[$_REQUEST['path']]."/".$img_filename;
            }

			//Заменяем данные в шаблоне
            $cur_template = str_replace("{IMG}",$img_path, $template);
			$cur_template = str_replace("{NUM1}",$num1, $cur_template);
			$cur_template = str_replace("{NUM2}",$num2, $cur_template);
            $cur_template = str_replace("{TEXT}",$text_content , $cur_template);
			$cur_template = str_replace("{CAPTION}",$caption , $cur_template);
            $result_file .=$cur_template;
            $file_count++;
        }
    }

	//пишем
    wfile("output/index.html", $result_file);
    draw_message("Parcing files finished. Proceeded: $file_count files");
}

/**
 * Return structure of object
 * @param string $obj
 * @return html
 */
function d($obj,$type = 'dump')
{
    if($type == 'dump')
        return var_dump($obj);
    else
        return print_r($obj);
}

/**
 * Get list of all files from specific directory
 * @param string $dir
 * @param string $file_ext
 * @param boolean $isfile
 * @return array
 */
function get_dirlist($dir,$file_ext,$isfile = true)
{
    $ret=array();
     if(is_dir($dir))
      {
        if($handle = opendir($dir))
        {
          while(($file = readdir($handle)) !== false)
          {
            if($file != "." && $file != ".." && $file != "Thumbs.db" && $file != "index.php"/*pesky windows, images..*/)
            {
                if ($isfile == false)
                {
                    if(strrpos($file, ".") == false)
                        array_push($ret, $file);
                }
                else
                {
                    if (substr($file, strlen($file)-strlen($file_ext)) == $file_ext)
                        array_push($ret, $file);
                }
            }
          }
          closedir($handle);
          return $ret;
        }
      }
}

/**
 * Read contents from file
 * @param string $filename
 * @return string
 */
function rfile($filename)
{
    $handle = fopen($filename, "r");
    $contents = stream_get_contents($handle);
    fclose($handle);
    return $contents;
}

/**
 * Write content to file
 * @param string $filename
 * @param string $content
 */
function wfile($filename,$content)
{
    $fp = fopen($filename, "w");
    fputs ($fp, "$content");
    fclose ($fp);
}
/**
 * Delete a directory
 * @param string $directory
 * @param string $empty
 * @return bool 
 */
function remove_directory($directory, $empty=FALSE)
 {
     // if the path has a slash at the end we remove it here
     if(substr($directory,-1) == '/')
    {
         $directory = substr($directory,0,-1);
     }
  
     // if the path is not valid or is not a directory ...
     if(!file_exists($directory) || !is_dir($directory))
     {
         // ... we return false and exit the function
         return FALSE;
  
     // ... if the path is not readable
     }elseif(!is_readable($directory))
     {
         // ... we return false and exit the function
         return FALSE;
  
     // ... else if the path is readable
     }else{
  
         // we open the directory
         $handle = opendir($directory);
  
         // and scan through the items inside
         while (FALSE !== ($item = readdir($handle)))
         {
             // if the filepointer is not the current directory
             // or the parent directory
             if($item != '.' && $item != '..')
             {
                 // we build the new path to delete
                 $path = $directory.'/'.$item;
  
                 // if the new path is a directory
                 if(is_dir($path)) 
                 {
                     // we call this function with the new path
                     remove_directory($path);
  
                 // if the new path is a file
                 }else{
                     // we remove the file
                    unlink($path);
                 }
             }
         }
         // close the directory
         closedir($handle);
  
         // if the option to empty is not set to true
        if($empty == FALSE)
         {
             // try to delete the now empty directory
             if(!rmdir($directory))
             {
                 // return false if not possible
                 return FALSE;
             }
         }
         // return success
         return TRUE;
     }
 }
 
 
function SaveAsHtmlByWord($filename,$new_filename)
{
 /*   $word = new COM("word.application") or die("Unable to instantiate Word");
    $word->Documents->Open("F:\\Servers\\xampp\\htdocs\\Php_stuff\\RGAZU_LIBRAY_PARSER\\raw_files\\Untitled71.rtf");
    echo $filename;
    $word->Visible = 1;
    print_r( $word->Documents);
    print_r( $word->Documents[1]);
    
    //$new_filename = substr($filename,0,-4) . ".htm";
    $word->Documents[1]->SaveAs("F:/1.txt",2);
    $word->Documents[1]->Close(false);
    $word->Quit();
    $word->Release();
    $word = NULL;
    unset($word);
    
    */
    // starting word
$word = new COM("word.application") or die("Unable to instantiate Word");
echo "Loaded Word, version {$word->Version}\n";
$word->Documents->Open("F:\\Servers\\xampp\\htdocs\\Php_stuff\\RGAZU_LIBRAY_PARSER\\raw_files\\Untitled71.doc");
    echo $filename;
    $word->Visible = 1;
    print_r( $word->Documents);
    print_r( $word->Documents[1]);
    
//bring it to front
$word->Visible = 1;

//open an empty document
#$word->Documents->Add();

//do some weird stuff
#$word->Selection->TypeText("This is a test...");
$word->Documents[1]->SaveAs("F:/1.htm",8);

//closing word
$word->Quit();

//free the object
$word = null;
}

?>