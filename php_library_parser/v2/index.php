<?php
/**
 * RGAZU Library Files Parser v2
 * by Eremin Andrey
 * 2012. All Rights Reserved.
 * http://eremin.me
 */
date_default_timezone_set('Europe/Moscow'); 
require 'core.php';

//settings
$version = '2.1.0';
$url = "/img/library/newentries/"; //url where to store files on server
$html_ext = "htm"; //extention for HTML files with data
$template = 'library_010612.html'; // current template name
$index_template = 'library_index_051212.html';
$folders = Array(
    Array("server_side" => "agriculture", "id" => "0", "name" => "Сельское хозяйство", "index_id" => 924), 
    Array("server_side" => "applied_science", "id" => "1", "name" => "Unknown", "index_id" => 923),
    Array("server_side" => "economics", "id" => "2", "name" => "Экономические науки", "index_id" => 918),
    Array("server_side" => "jurisprudence", "id" => "3", "name" => "Юридические науки", "index_id" => 925),
    Array("server_side" => "other", "id" => "4", "name" => "Другие науки", "index_id" => 953),
    Array("server_side" => "science", "id" => "5", "name" => "Естественные науки", "index_id" => 922),
    Array("server_side" => "socialscience", "id" => "6", "name" => "Общественные науки", "index_id" => 917),
    Array("server_side" => "tech", "id" => "7", "name" => "Технические науки", "index_id" => 920)
); #Array of folders whick will be used for the Parser

//============================================================
# INTERFACE
echo "
      <link rel='stylesheet' href='stylesheets/base.css'>
      <link rel='stylesheet' href='stylesheets/skeleton.css'>
      <link rel='stylesheet' href='stylesheets/layout.css'>
      <div class = 'container'>
      ";
echo "<h3>RGAZU Library Files Parser v$version</h3>";
    
if(isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "clean":
            clean_folders();
            break;
        case "parse":
            parse_files($url,$folders,$template,$index_template,$_REQUEST["date"]);
            break;
        case "check":
            check_files($folders);
            break;          
    }
}

draw_interface();

echo "</div>";


//============================================================

/**
 * Draw an interface
 */
function draw_interface()
{
    echo "      
      <form>
          <input type='hidden' name='action' value='clean'/>
          1. <input type='submit' value='Prepare folders'/>
          <br/><i>Directories 'output' and 'raw' will be cleaned out</i>
      </form>
      <form>
          <input type='hidden' name='action' value='check'/>
          2. <input type='submit' value='Verify files'/>
          <br/><i>Files stored in 'raw' directory will be verified</i>
      </form> 
      <form>
          3. <input type='submit' value='Parse files'/><br/>
          <input type='text' name='date' value='" . date("d.m.Y") . "'>
          <input type='hidden' name='action' value='parse'/> 
          <i>Files stored in 'raw' directory will be parsed with date value from the input</i>
      </form>
    ";   
}

/**
*   Removes all the files and folders from 'raw' and 'output' directories
*/
function  clean_folders()
{
    $parser = new ParserCore(Array());
    $result = $parser->clean_folders();
    if($result['status'] == true)
        message("All files were successfully deleted.","success");
    else
        message("Errors occured during the process. Found following errors: <br/>".$result['errors'],"error");
}

/**
* Parce files
*/
function parse_files($url,$folders,$template,$index_template,$date)
{
    $parser = new ParserCore(Array("folders"=>$folders, "url"=>$url, "template" => $template, "index_template" => $index_template, "date" => $date));
    $result = $parser->parse();
    if($result['status'] == true)
        message("All files were successfully parsed. Check out 'output' directory. ".$result['stat'],"success");
    else
        message("Errors occured during the process. Found following errors: <br/>".$result['errors'],"error");
}

/**
*   Check if all the files are in the good conditions to be parsed
*/

function  check_files($folders)
{
    $parser = new ParserCore(Array("folders"=>$folders));
    $result = $parser->check_folders();
    if($result['status'] == true)
        message("All files were successfully verified.","success");
    else
        message("Errors occured during the verification process. Found following errors: <br/>".$result['errors'],"error");
}

/**
*   Renders info message
*/
function message($text, $type = "info")
{
    $color = "blue";
    switch($type){
        case "info":
            $color = "blue";
            break;
        case "error":
            $color = "red";
            break;
        case "success":
            $color = "green";
            break;
    }
    echo "<div style='color:$color;'>$text</div>";
}

?>