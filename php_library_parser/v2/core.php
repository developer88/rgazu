<?php
/*
* Parser Core Class
*/
Class ParserCore
{

	public $options = Array();
	public $separator = "/";
	function __construct($options_arr) {
		date_default_timezone_set('Europe/Moscow');	
		$this->options = $options_arr;
		if(!isset($this->options['date']) || strlen($this->options['date']) < 10){
			$this->options['date'] = date("d.m.Y");
		}
		if ("Windows" === PHP_OS)
		{
		    $this->separator = "\\";
		}else{
			$this->separator = "/";
		}		
	}
	
	public function check_folders(){
		$this->check_options("check");
		#This will check all the folders and files inside 'raw' directory
		try {
			$errors = "";
			$root_folders = $this->get_dirlist("raw".$this->separator,"",false);
			for($i = 0; $i < count($root_folders); $i++){
				$folders = $this->get_dirlist("raw".$this->separator.$root_folders[$i].$this->separator,"",false);
				# 1. Match if all folders from 'raw' directory are exist in options 'folders' array
				$folders_result = $this->match_folders($folders, $this->options['folders']);
				# 2. Check if inside each directory in 'raw' there are only image files and texts files
				$files_result = $this->match_files($folders, $root_folders[$i]);
				if($folders_result['status'] == false || $files_result['status'] == false){
					if($folders_result['status'] == false)
						$errors .= $folders_result['errors'];
						
					if($files_result['status'] == false)
						$errors .= $files_result['errors'];				
				}					
			}			
			if(strlen($errors) == 0){
				return Array("status" => true);
			} else {
				return Array("status" => false, "errors" => $errors);			
			}		
		} catch (Exception $e) {
			return Array("status" => false, "errors" => $e->getMessage());
		}	
	}
	
	public function parse(){
	    $this->check_options("parse");
		try {
		    $files_proceed = 0;
			$folders_proceed = 0;
			# 0. Clear out the output directory
			$this->clean_output_directory();
			# 1. Check folders inside 'raw' directory
			$check_result = $this->check_folders();
			if($check_result['status'] == true){
				# 2. Parse
				$root_folders = $this->get_dirlist("raw".$this->separator,"",false);
				for($i = 0; $i < count($root_folders); $i++){
				    $folders_proceed++;
					mkdir("output".$this->separator.$root_folders[$i].$this->separator);
					$subfolders = $this->get_dirlist("raw".$this->separator.$root_folders[$i].$this->separator,"",false);
					for($j = 0; $j < count($subfolders); $j++){
					    $folders_proceed++;
						$folder = $this->get_folder_var($subfolders[$j]);
						if(count($folder)>0){
							mkdir($this->get_app_dir()."output".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j]);
							mkdir($this->get_app_dir()."output".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator.$folder['server_side']);
							$template = $this->load_template();
							$html = "";
							$files = $this->get_dirlist("raw".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator,"txt",true);
							for($m = 0; $m < count($files); $m++){
							    $files_proceed+=2;
								$image = $this->get_image_from_filename($files[$m]);
								if(file_exists("raw".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator.$image)){								
									$new_image = $this->new_image_name($image);								
									copy($this->get_app_dir()."raw".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator.$image, $this->get_app_dir()."output".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator.$folder['server_side'].$this->separator.$new_image);
									$html .= $this->fill_template($template, "raw".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator.$files[$m], $new_image, $folder['server_side']);								
								} else {
									return Array("status" => false, "errors" => "no file found '"."raw".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator.$image."'");
									//$this->d("raw".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator.$image);
								}
							}	
							# Prepare index file
							$index_template = $this->load_template(true);
							$index_html = $this->fill_index_template($index_template, $this->options['date'], $html, $folder['index_id'], $folder['name']);
							# Writes index file
							$this->wfile("output".$this->separator.$root_folders[$i].$this->separator.$subfolders[$j].$this->separator."index.html",$index_html);							
						} else {
							return Array("status" => false, "errors" => "no info about '".$subfolders[$j]."'");
						}					
					}			
				}
				# 3. Finish
				return Array("status" => true, "stat" => "files proceed: $files_proceed, folders proceed: $folders_proceed");
			}else{
				return $check_result;
			}
		} catch (Exception $e) {
			return Array("status" => false, "errors" => $e->getMessage());
		}	
	}
	
	public function clean_folders(){ 
		try {
			if(file_exists($this->get_app_dir()."raw".$this->separator))
				$this->remove_directory($this->get_app_dir()."raw".$this->separator);
				
			if(file_exists($this->get_app_dir()."output".$this->separator))
				$this->remove_directory($this->get_app_dir()."output".$this->separator);

			if(!file_exists($this->get_app_dir()."raw".$this->separator))
				mkdir($this->get_app_dir()."raw".$this->separator);	

			if(!file_exists($this->get_app_dir()."output".$this->separator))
				mkdir($this->get_app_dir()."output".$this->separator);
			
			return Array("status" => true);			
		} catch (Exception $e) {
			return Array("status" => false, "errors" => $e->getMessage());
		}	
	}
	
	//======================================================================================
	
	/**
	 *	Gets folder info by name from 'folders' variable
	 */
	private function get_folder_var($name)
	{
		for($i = 0; $i < count($this->options['folders']); $i++){
			if($this->options['folders'][$i]['name'] == $name)
				return $this->options['folders'][$i];
		}
		return Array();
	}
	
	/**
	 * Cleans out the output directory
	 */
	private function clean_output_directory()
	{
		if(file_exists("output".$this->separator))
			$this->remove_directory("output".$this->separator);	
		if(!file_exists("output".$this->separator))
			mkdir("output".$this->separator);	
	}
	
	/**
	 *	Removes . from filename and adds .jpg extension
	 */
	private function get_image_from_filename($filename){
		$data_arr = preg_split("/\./",$filename);	
		array_pop($data_arr);
		return implode(".", $data_arr).".jpg";
	}
	
	/**
	 *	Generate new image name and adds .jpg extension
	 */
	private function new_image_name($filename){
		return  rand( 0 , 123 ).strlen($filename).rand( 0 , 321).date('YmdGi').".jpg";
	}	
	
	/**
	 * Fills template html with data from file
	 */
	private function fill_template($template,$filename,$image_name,$foldername)
	{
		$data = $this->rfile($filename);	
		$data = str_replace("\r\n", "splt", $data);
		$data_arr = preg_split("/splt/",$data);	
		$template = str_replace("{IMG}", $this->options['url'].$foldername."/".$image_name, $template);
		$template = str_replace("{NUM1}", $data_arr[0], $template);
		$template = str_replace("{NUM2}", $data_arr[1], $template);			
		$caption =  $data_arr[2];
		if(count($data_arr) > 3)
			$caption.=" ".$data_arr[3];
			
		$text = "";
		if(count($data_arr) > 4){
			array_shift($data_arr);
			array_shift($data_arr);
			array_shift($data_arr);
			array_shift($data_arr);	
			$text = implode("\r\n", $data_arr);
		}

		$template = str_replace("{CAPTION}", $caption, $template);
		$template = str_replace("{TEXT}", $text, $template);
		return $template;
	}

	/**
	 * Fills index template html with data 
	 */
	private function fill_index_template($template, $date, $items, $index_id, $cat_name)
	{
		$template = str_replace("{INDEX_ID}", $index_id, $template);
		$template = str_replace("{DATE}", $date, $template);
		$template = str_replace("{ITEMS}", $items, $template);
		$template = str_replace("{CAT_NAME}", iconv (  'UTF-8','Windows-1251' , $cat_name), $template);		
		return $template;
	}
	
	/**
	 * Loads template
	 */
	private function load_template($for_index = false)
	{
		try {

			$template_name = $for_index == false ? $this->options['template'] : $this->options['index_template'];
			$template = $this->rfile("templates".$this->separator.$template_name);	
			if(strlen($template) < 5)
				throw new Exception("Template is empty: $template_name");
			return $template;
		} catch (Exception $e) {
			throw new Exception("Error occured while loading template: ".$e->getMessage());
		}
	}
	
	/**
	 *	Check option variable
	 */
	 private function check_options($action)
	 {
		$options_missed = "";
		switch($action){
			case "check":
				if(count($this->options['folders']) == 0){$options_missed .= "folders; ";};
				break;
			case "clean":			
				break;
			case "parse":
				if(count($this->options['folders']) == 0){$options_missed .= "folders; ";};
				if(strlen($this->options['url']) == 0){$options_missed .= "url; ";};
				if(strlen($this->options['template']) == 0){$options_missed .= "template; ";};
				break;
		}
		if(strlen($options_missed) > 0)
			 throw new Exception("Following options variables are missed: $options_missed");
		
		return true;
	 }
	 
	/**
	 * Matches folders from 'raw' directory whith options 'folders' variable
	 */
	private function match_folders($raw_folders, $options_folders)
	{
		for($i = 0; $i < count($raw_folders); $i++){
			$inside = false;
			for($j = 0; $j < count($options_folders); $j++){
				if($this->compare_with_encoding($options_folders[$j]['name'], $raw_folders[$i]))
					$inside = true;	
			}
			if(!$inside && $raw_folders[$i] != ".DS_Store")
				return Array("status" => false, "errors" => "unknown directory name '".$raw_folders[$i]."'");		
		}
		return Array("status" => true);	
	}

	/**
	 * Compares 2 variables but change its emcoding before
	 */
	private function compare_with_encoding($left, $right)
	{
		if(mb_detect_encoding($left) != mb_detect_encoding($right)){
			//$left = mb_convert_encoding ( $left, mb_detect_encoding($right));
		}
		if($left == $right)
			return true;
		else
			return false;
	}
	
	/**
	 * Checks if inside raw_folders there are only images and text files
	 */
	private function match_files($raw_folders,$root)
	{
		for($i = 0; $i < count($raw_folders); $i++){
			$images = $this->get_dirlist("raw".$this->separator.$root.$this->separator.$raw_folders[$i],"jpg",true);
			$texts = $this->get_dirlist("raw".$this->separator.$root.$this->separator.$raw_folders[$i],"txt",true);
			$all = $this->get_dirlist("raw".$this->separator.$root.$this->separator.$raw_folders[$i],"",true);
			if(count($all) != count($images) + count($texts))
				return Array("status" => false, "errors" => "unwanted files inside ".$raw_folders[$i]." directory");		
		}
		return Array("status" => true);
	}
	 
	/**
	 * Return structure of object
	 * @param string $obj
	 * @return html
	 */
	private function d($obj,$type = 'dump')
	{
		$obj = iconv ( 'Windows-1251' , 'UTF-8' , $obj );
		if($type == 'dump')
			return var_dump($obj);
		else
			return print_r($obj);
	}

	/**
	 * Gets full app dir
	 **/
	private function get_app_dir(){
		return $_SERVER['DOCUMENT_ROOT'] . str_replace("index.php","",$_SERVER['PHP_SELF']);
	}

	/**
	 * Get list of all files from specific directory
	 * @param string $dir
	 * @param string $file_ext
	 * @param boolean $isfile
	 * @return array
	 */
	private function get_dirlist($dir,$file_ext,$isfile = true)
	{
		$path =  $_SERVER['DOCUMENT_ROOT'] . str_replace("index.php","",$_SERVER['PHP_SELF']) . $this->separator . $dir;
		#echo $path."!\n\r";
		#echo $dir;
		#echo $this->separator;
		#echo $_SERVER['PHP_SELF'];
		$ret=array();
		 if(is_dir($path))
		  {		  
			if($handle = opendir($path))
			{
			  while(($file = readdir($handle)) !== false)
			  {
				if($file != "." && $file != ".." && $file != "Thumbs.db" && $file != "index.php" && $file != ".DS_Store"
					/*pesky windows, images..*/)
				{
					if ($isfile == false)
					{
						//if(strrpos($file, ".") == false)
							array_push($ret, $file);
					}
					else
					{
						if (substr($file, strlen($file)-strlen($file_ext)) == $file_ext)
							array_push($ret, $file);
					}
				}
			  }
			  closedir($handle);
			  return $ret;
			}
		  }
	}

	/**
	 * Read contents from file
	 * @param string $filename
	 * @return string
	 */
	private function rfile($filename)
	{
		$handle = fopen($filename, "r");
		$contents = stream_get_contents($handle);
		fclose($handle);
		return $contents;
	}

	/**
	 * Write content to file
	 * @param string $filename
	 * @param string $content
	 */
	private function wfile($filename,$content)
	{
		$fp = fopen($filename, "w");
		fputs ($fp, "$content");
		fclose ($fp);
	}
	
	/**
	 * Delete a directory
	 * @param string $directory
	 * @param string $empty
	 * @return bool 
	 */
	private function remove_directory($directory, $empty=FALSE)
	 {
		 // if the path has a slash at the end we remove it here
		 if(substr($directory,-1) == '/')
		 {
			 $directory = substr($directory,0,-1);
		 }
	  
		 // if the path is not valid or is not a directory ...
		 if(!file_exists($directory) || !is_dir($directory))
		 {
			 // ... we return false and exit the function
			 return FALSE;
	  
		 // ... if the path is not readable
		 }elseif(!is_readable($directory))
		 {
			 // ... we return false and exit the function
			 return FALSE;
	  
		 // ... else if the path is readable
		 }else{
	  
			 // we open the directory
			 $handle = opendir($directory);
	  
			 // and scan through the items inside
			 while (FALSE !== ($item = readdir($handle)))
			 {
				 // if the filepointer is not the current directory
				 // or the parent directory
				 if($item != '.' && $item != '..' && $item != ".DS_Store")
				 {
					 // we build the new path to delete
					 $path = $directory.'/'.$item;
	  
					 // if the new path is a directory
					 if(is_dir($path)) 
					 {
						 // we call this function with the new path
						 $this->remove_directory($path);
	  
					 // if the new path is a file
					 }else{
						 // we remove the file
						unlink($path);
					 }
				 }
			 }
			 // close the directory
			 closedir($handle);
	  
			 // if the option to empty is not set to true
			if($empty == FALSE)
			 {
				 // try to delete the now empty directory
				 if(!rmdir($directory))
				 {
					 // return false if not possible
					 return FALSE;
				 }
			 }
			 // return success
			 return TRUE;
		 }
	 }
	 
}

?>