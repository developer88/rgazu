/* 
 * JS Gallery Generator Library for RGAZU
 * by Eremin Andrey aka Developer, 2011
 * website: http://eremin.me
 */

function DevGallery() {
    this.url = "http://www.rgazu.ru/img/news/";
    this.min_prefix = "_s";
    this.draw_gallery = function(div_id,dir_name,start,end){
       var html = "";
       if(div_id.length ==0 || dir_name.length ==0 || parseInt(start) > parseInt(end)) {
           return "";
       }
       var i = 0;
       for(i = parseInt(start); i < parseInt(end+1); i++) {
           html += '<a href="' + this.url + dir_name + '/' + this.get3DigitValueStr(i) + '.jpg" rel="lightbox"><img src="' + this.url + dir_name + '/' + this.get3DigitValueStr(i) + this.min_prefix + '.jpg" border="0" alt="Нажмите для увеличения" /> </a>'
       }
       $('#' + div_id).html(html);        
    }  
    this.get3DigitValueStr = function(digit) {
        digit = digit.toString();
        var length_to_add = 3 - digit.length;
        if(length_to_add > 0) {
            var added_nulls = '';
            for(var i = 0; i < length_to_add; i++){
               added_nulls += '0'; 
            }
            digit = added_nulls + digit;
        }
        return digit;
    }
}